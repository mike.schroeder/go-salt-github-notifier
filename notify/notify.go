package notify

import (
	"regexp"
)

func ValidRef(refs *[]*regexp.Regexp, test string) bool {
	for _, ref := range *refs {
		if ref.MatchString(test) {
			return true
		}
	}

	return false
}

func ValidRepo(refs *[]*regexp.Regexp, test string) bool {
	for _, repo := range *refs {
		if repo.MatchString(test) {
			return true
		}
	}

	return false
}
