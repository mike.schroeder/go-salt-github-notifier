package cli

import (
	"os/exec"
	"path/filepath"
	"strconv"
)

type Salt struct {
	cmdFileClearCache *exec.Cmd
	cmdFileUpdate     *exec.Cmd
	cmdPillarUpdate   *exec.Cmd
	cmdSyncAll        *exec.Cmd
	cmdMineUpdate     *exec.Cmd
}

func NewSalt(path string, batch int) *Salt {

	s := &Salt{}
	s.cmdFileClearCache = exec.Command(filepath.Join(path, "salt-run"), "fileserver.clear_cache")
	s.cmdFileUpdate = exec.Command(filepath.Join(path, "salt-run"), "fileserver.update")
	s.cmdPillarUpdate = exec.Command(filepath.Join(path, "salt-run"), "git_pillar.update")
	s.cmdMineUpdate = exec.Command(filepath.Join(path, "salt"), "*", "-b", strconv.Itoa(batch), "mine.update")
	s.cmdSyncAll = exec.Command(filepath.Join(path, "salt"), "*", "-b", strconv.Itoa(batch), "saltutil.sync_all")

	return s
}

func (s Salt) FileCacheClear() error {
	return s.cmdFileClearCache.Run()
}

func (s Salt) FileUpdate() error {
	return s.cmdFileUpdate.Run()
}
func (s Salt) PillarUpdate() error {
	return s.cmdPillarUpdate.Run()
}
func (s Salt) MineUpdate() error {
	return s.cmdMineUpdate.Run()
}
func (s Salt) SyncAll() error {
	return s.cmdSyncAll.Run()
}

func (s Salt) RunAll() error {
	err := s.cmdFileClearCache.Run()
	if err != nil {
		return err
	}

	err = s.cmdFileUpdate.Run()
	if err != nil {
		return err
	}

	err = s.cmdPillarUpdate.Run()
	if err != nil {
		return err
	}

	err = s.cmdMineUpdate.Run()
	if err != nil {
		return err
	}

	err = s.cmdSyncAll.Run()
	if err != nil {
		return err
	}

	return nil
}
